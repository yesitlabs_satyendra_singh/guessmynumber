<?php
include 'config.php';
$q = $pdo->prepare('SELECT * FROM `users` WHERE `id`=?');
$q->execute(array($_GET['user']));
$data = $q->fetch(PDO::FETCH_ASSOC);

if (strtolower(date('A')) == 'am') {
    $number = $data['number12am'];
} else {
    $number = $data['number12pm'];
}

$number_parts = str_split($number);
$guesses = json_decode($data['guesses'], 1);

if (
    @$guesses[0] . @$guesses[1] . @$guesses[2] == $number ||
    @$guesses[3] . @$guesses[4] . @$guesses[5] == $number ||
    @$guesses[6] . @$guesses[7] . @$guesses[8] == $number
) {
    $won = 1;
}

if (empty($won) && $data['currentRow'] > 2) {
    $lost = 1;
}

$content = '';

$content .= '<table style="width: 100%; border-spacing: 5px; border-collapse: separate;">
        <tr id="row_1"><td>';

if (empty($data['tile_1'])) {
    $content .= '<div class="box box-white"><div class="content"><span style="opacity: 0;">0</span></div></div>';
} else {
    $box = json_decode($data['tile_1'], 1);
    $content .= '<div class="box ' . $box['className'] . ' ' . $box['animation'] . '"><div class="content">' . $box['value'] . '</div></div>';

    $box['animation'] = '';
    $q = $pdo->prepare('UPDATE `users` SET `tile_1`=? WHERE `id`=?');
    $q->execute(array(json_encode($box), $_GET['user']));
}

$content .= '</td><td>';

if (empty($data['tile_2'])) {
    $content .= '<div class="box box-white"><div class="content"><span style="opacity: 0;">0</span></div></div>';
} else {
    $box = json_decode($data['tile_2'], 1);
    $content .= '<div class="box ' . $box['className'] . ' ' . $box['animation'] . '"><div class="content">' . $box['value'] . '</div></div>';

    $box['animation'] = '';
    $q = $pdo->prepare('UPDATE `users` SET `tile_2`=? WHERE `id`=?');
    $q->execute(array(json_encode($box), $_GET['user']));
}

$content .= '</td><td>';

if (empty($data['tile_3'])) {
    $content .= '<div class="box box-white"><div class="content"><span style="opacity: 0;">0</span></div></div>';
} else {
    $box = json_decode($data['tile_3'], 1);
    $content .= '<div class="box ' . $box['className'] . ' ' . $box['animation'] . '"><div class="content">' . $box['value'] . '</div></div>';

    $box['animation'] = '';
    $q = $pdo->prepare('UPDATE `users` SET `tile_3`=? WHERE `id`=?');
    $q->execute(array(json_encode($box), $_GET['user']));
}

$content .= '</td></tr></table>';

$content .= '<table style="width: 100%; border-spacing: 5px; border-collapse: separate;">
        <tr id="row_2"><td>';

if (empty($data['tile_4'])) {
    $content .= '<div class="box box-white"><div class="content"><span style="opacity: 0;">0</span></div></div>';
} else {
    $box = json_decode($data['tile_4'], 1);
    $content .= '<div class="box ' . $box['className'] . ' ' . $box['animation'] . '"><div class="content">' . $box['value'] . '</div></div>';

    $box['animation'] = '';
    $q = $pdo->prepare('UPDATE `users` SET `tile_4`=? WHERE `id`=?');
    $q->execute(array(json_encode($box), $_GET['user']));
}

$content .= '</td><td>';

if (empty($data['tile_5'])) {
    $content .= '<div class="box box-white"><div class="content"><span style="opacity: 0;">0</span></div></div>';
} else {
    $box = json_decode($data['tile_5'], 1);
    $content .= '<div class="box ' . $box['className'] . ' ' . $box['animation'] . '"><div class="content">' . $box['value'] . '</div></div>';

    $box['animation'] = '';
    $q = $pdo->prepare('UPDATE `users` SET `tile_5`=? WHERE `id`=?');
    $q->execute(array(json_encode($box), $_GET['user']));
}

$content .= '</td><td>';

if (empty($data['tile_6'])) {
    $content .= '<div class="box box-white"><div class="content"><span style="opacity: 0;">0</span></div></div>';
} else {
    $box = json_decode($data['tile_6'], 1);
    $content .= '<div class="box ' . $box['className'] . ' ' . $box['animation'] . '"><div class="content">' . $box['value'] . '</div></div>';

    $box['animation'] = '';
    $q = $pdo->prepare('UPDATE `users` SET `tile_6`=? WHERE `id`=?');
    $q->execute(array(json_encode($box), $_GET['user']));
}

$content .= '</td></tr></table>';

$content .= '<table style="width: 100%; border-spacing: 5px; border-collapse: separate;">
        <tr id="row_3"><td>';

if (empty($data['tile_7'])) {
    $content .= '<div class="box box-white"><div class="content"><span style="opacity: 0;">0</span></div></div>';
} else {
    $box = json_decode($data['tile_7'], 1);
    $content .= '<div class="box ' . $box['className'] . ' ' . $box['animation'] . '"><div class="content">' . $box['value'] . '</div></div>';

    $box['animation'] = '';
    $q = $pdo->prepare('UPDATE `users` SET `tile_7`=? WHERE `id`=?');
    $q->execute(array(json_encode($box), $_GET['user']));
}

$content .= '</td><td>';

if (empty($data['tile_8'])) {
    $content .= '<div class="box box-white"><div class="content"><span style="opacity: 0;">0</span></div></div>';
} else {
    $box = json_decode($data['tile_8'], 1);
    $content .= '<div class="box ' . $box['className'] . ' ' . $box['animation'] . '"><div class="content">' . $box['value'] . '</div></div>';

    $box['animation'] = '';
    $q = $pdo->prepare('UPDATE `users` SET `tile_8`=? WHERE `id`=?');
    $q->execute(array(json_encode($box), $_GET['user']));
}

$content .= '</td><td>';

if (empty($data['tile_9'])) {
    $content .= '<div class="box box-white"><div class="content"><span style="opacity: 0;">0</span></div></div>';
} else {
    $box = json_decode($data['tile_9'], 1);
    $content .= '<div class="box ' . $box['className'] . ' ' . $box['animation'] . '"><div class="content">' . $box['value'] . '</div></div>';

    $box['animation'] = '';
    $q = $pdo->prepare('UPDATE `users` SET `tile_9`=? WHERE `id`=?');
    $q->execute(array(json_encode($box), $_GET['user']));
}

$content .= '</td></tr></table>';

if (isset($won)) {
    $arr = array();
    $arr['won'] = 1;
    $arr['content'] = $content;
}

if (isset($lost)) {
    $arr = array();
    $arr['lost'] = 1;
    $arr['content'] = $content;
}

if (isset($won) && empty($_SESSION['statistics_title'])) {
    $q = $pdo->prepare('UPDATE `users` SET `gamesWon`=`gamesWon`+1 WHERE `id`=?');
    $q->execute(array($_GET['user']));
}

if (empty($won) && empty($lost)) {
    $arr = array();
    $arr['content'] = $content;
}

echo json_encode($arr);
