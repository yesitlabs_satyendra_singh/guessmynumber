<?php
include 'config.php';

$q = $pdo->prepare('SELECT * FROM `number_queue` WHERE `status`=\'active\' ORDER BY `id` DESC');
$q->execute();
foreach ($q as $row) {

	if ($row['queue_time'] < time()) {

		if (date('A', $row['queue_time']) == 'AM') {
			$set_number = $pdo->prepare('UPDATE `settings` SET `am_number`=?, `queue_data`=?');
			$set_number->execute(array($row['number'], json_encode($row)));
			$am_pm = 'am';
		} else {
			$set_number = $pdo->prepare('UPDATE `settings` SET `pm_number`=?, `queue_data`=?');
			$set_number->execute(array($row['number'], json_encode($row)));
			$am_pm = 'pm';
		}

		$q__ = $pdo->prepare('UPDATE `number_queue` SET `status`=\'finished\' WHERE `id`=?');
		$q__->execute(array($row['id']));

		$q__ = $pdo->prepare('SELECT * FROM `settings` WHERE `id`=1');
		$q__->execute();
		$settings = $q__->fetch(PDO::FETCH_ASSOC);

		$q__ = $pdo->prepare('UPDATE `users` SET `tile_1`=\'\', `tile_2`=\'\', `tile_3`=\'\', `tile_4`=\'\', `tile_5`=\'\', `tile_6`=\'\', `tile_7`=\'\', `tile_8`=\'\', `tile_9`=\'\', `am_pm`=?, `guesses`=\'\', `number12am`=?, `number12pm`=?, `currentRow`=0');
		$q__->execute(array($am_pm, $settings['am_number'], $settings['pm_number']));
	}
}
