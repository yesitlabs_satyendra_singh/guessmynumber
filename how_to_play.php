<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>How To Play | Guess My Number</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mt-3">
                <p class="text-justify">
                    Congratulations! You guessed today's number! Hizzah! Please fill out the form in the
                    super-secret VIP SECTION with YOUR numbers for the world to guess. You can be
                    specific or vague with your location, so don't worry. If you include your email, you'll be
                    the first to know some of our fun updates in the future as part of our VIP WINNERS
                    CLUB. Due to the high volume of submissions, we cannot guarantee your number will be
                    picked. However, the more you play every day, the higher the chance of being featured.
                    If you have any questions, please email us or tweet us. Thanks again! And, as Rockford T.
                    Honeypot would say, "CHEERS TO BOTH EARS!"
                </p>
            </div>
        </div>
    </div>
</body>

</html>