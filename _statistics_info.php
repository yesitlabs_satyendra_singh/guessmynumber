<?php
include 'config.php';

$q = $pdo->prepare('SELECT * FROM `settings` WHERE `id`=1');
$q->execute();
$settings = $q->fetch(PDO::FETCH_ASSOC);

if ($_GET['correct_row'] < 1) {
	$wrong_percent = ($_GET['total_wrong'] / 9) * 100;

	if ($wrong_percent < 33) {
		$data = json_decode($settings['lost_33'], 1);
	}
	if ($wrong_percent >= 33) {
		$data = json_decode($settings['lost_66'], 1);
	}
	if ($wrong_percent >= 66) {
		$data = json_decode($settings['lost_100'], 1);
	}
} else {
	$data = json_decode($settings['won_row_' . $_GET['correct_row']]);
}

if (isset($_SESSION['statistics_title'])) {
	echo $_SESSION['statistics_title'];
} else {
	$result_title = $data[array_rand($data)];
	$_SESSION['statistics_title'] = $result_title;
	echo $result_title;
}
