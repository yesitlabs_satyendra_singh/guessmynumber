<?php
$rem = $_GET['timestamp'] - time();
$day = sprintf("%02d", floor($rem / 86400));
$hr  = sprintf("%02d", floor(($rem % 86400) / 3600));
$min = sprintf("%02d", floor(($rem % 3600) / 60));
$sec = sprintf("%02d", ($rem % 60));
echo json_encode(['h' => $hr, 'm' => $min, 's' => $sec]);
