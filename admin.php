<?php
include 'config.php';
$q = $pdo->prepare('SELECT * FROM `settings` WHERE `id`=1');
$q->execute();

$getallsubmittednumber = $pdo->prepare('SELECT * FROM `submittednumbers` ORDER BY ID DESC');
$getallsubmittednumber->execute();

$settings = $q->fetch(PDO::FETCH_ASSOC);
$all_submitted_numebr = $getallsubmittednumber->fetchAll(PDO::FETCH_ASSOC);

$rounds = 0;
$rounds_won = 0;

$player = $pdo->prepare('SELECT * FROM `users` WHERE `number12am`=? AND `number12pm`=?');
$player->execute(array($settings['am_number'], $settings['pm_number']));

foreach ($player as $row) {
    $rounds++;
    $am_pm = strtolower(date('A'));

    $parts = str_split($row['number12' . $am_pm]);

    if (strpos($row['guesses'], '"' . $parts[0] . '","' . $parts[1] . '","' . $parts[2] . '"') != false) {
        $rounds_won++;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    <title>Guess my number - Admin</title>
    <link rel="stylesheet" href="css/animate.css" />
    <style type="text/css">
        body {
            /*color: #5c05c8;*/
        }

        img {
            max-width: 100%;
        }

        .form-control {
            margin-bottom: 10px;
        }
    </style>
</head>

<body>

    <div class="container pt-4 pb-4">
        <h1>GUESS <span class="text-success">MY</span> NUMBERS - ADMIN</h1>
        <?php
        if (isset($_GET['logout'])) {
            session_destroy();
            header('Location: index.php');
            exit;
        }

        if (!empty($_POST['password'])) {
            if ($_POST['password'] == $settings['admin_password']) {
                $_SESSION['admin'] = 1;
                header('Location: joosherinohoneypot.php');
                exit();
            }
        }

        if (!empty($_SESSION['admin'])) {
            if (isset($_POST['save_admin_password'])) {
                $new_Admin_password = $_POST['add_new_password'];
                $sql1 = "UPDATE settings SET admin_password='" . $new_Admin_password . "' WHERE id=1";
                $pdo->query($sql1);
                header('Location: joosherinohoneypot.php?act=result_text');
            }
        ?>
            <link href="css/bootstrap.min.css" rel="stylesheet">
            <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
            <script src="js/bootstrap.bundle.min.js"></script>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#"></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarText">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="joosherinohoneypot.php">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="joosherinohoneypot.php?act=result_text">Result Text</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="joosherinohoneypot.php?act=submitted_numbers">Submitted Numbers</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="joosherinohoneypot.php?act=number_queue">Number Queue</a>
                            </li>
                        </ul>
                        <span class="navbar-text">
                            <a class="nav-link" id="change_admin_password" href="#">Change Admin Password</a>
                        </span>
                        <span class="navbar-text">
                            <a class="nav-link" href="joosherinohoneypot.php?logout=1">Logout</a>
                        </span>
                    </div>
                </div>
            </nav>
            <div class="modal fade" data-backdrop="false" tabindex="-1" role="dialog" id="change_password_modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Change Admin Password</h5>
                        </div>
                        <div class="modal-body">
                            <form action="" method="post">
                                Set New Password :
                                <input type="text" name="add_new_password">
                                <input type="submit" name="save_admin_password" class="btn btn-success">
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="modal_close" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                jQuery(document).ready(function() {
                    jQuery('#change_admin_password').click(function() {
                        jQuery('#change_password_modal').modal('show');
                    });
                    jQuery('#modal_close').click(function() {
                        jQuery('#change_password_modal').hide();
                        location.reload();

                    });
                });
            </script>
            <div class="p-4">
                <?php
                switch (@$_GET['act']) {
                    case 'result_text':

                        echo '<h2 class="text-muted">Result Text</h2><hr/>';

                        // if(!empty($_POST['won_row_1']))
                        // {
                        //   foreach ($_POST as $key => $value) {
                        //     $q = $pdo->prepare('UPDATE `settings` SET `'.$key.'`=? WHERE `id`=1');
                        //     $q->execute(array(json_encode($value)));
                        //     header('Location: joosherinohoneypot.php?act=result_text');
                        //     exit();
                        //   }
                        // }
                        // if(!empty($_POST['won_row_2']))
                        // {
                        //   foreach ($_POST['won_row_2'] as $key => $value) {
                        //     $q = $pdo->prepare('UPDATE `settings` SET `'.$key.'`=? WHERE `id`=1');
                        //     $q->execute(array(json_encode($value)));
                        //     header('Location: joosherinohoneypot.php?act=result_text');
                        //     exit();
                        //   }
                        // }

                        if (isset($_POST['submit_texts_for_game'])) {
                            $row1 = json_encode($_POST['won_row_1']);
                            $row2 = json_encode($_POST['won_row_2']);
                            $row3 = json_encode($_POST['won_row_3']);
                            $row4 = json_encode($_POST['lost_33']);
                            $row5 = json_encode($_POST['lost_66']);
                            $row6 = json_encode($_POST['lost_100']);

                            $sql = "UPDATE settings SET won_row_1='" . $row1 . "', won_row_2='" . $row2 . "', won_row_3='" . $row3 . "',lost_33='" . $row4 . "',lost_66='" . $row5 . "',lost_100='" . $row6 . "' WHERE id=1";
                            $pdo->query($sql);
                            header('Location: joosherinohoneypot.php?act=result_text');
                            exit();
                        }

                        echo '<form action="" method="post"><div class="row">';

                        $data = json_decode($settings['won_row_1'], 1);
                        echo '<div class="col-md-4"><h5>Won 1st row</h5>';
                        foreach ($data as $value) {
                            echo '<input type="text" class="form-control" name="won_row_1[]" value="' . $value . '"/>';
                        }
                        echo '</div>';

                        $data = json_decode($settings['won_row_2'], 1);
                        echo '<div class="col-md-4"><h5>Won 2nd row</h5>';
                        foreach ($data as $value) {
                            echo '<input type="text" class="form-control" name="won_row_2[]" value="' . $value . '"/>';
                        }
                        echo '</div>';

                        $data = json_decode($settings['won_row_3'], 1);
                        echo '<div class="col-md-4"><h5>Won 3rd row</h5>';
                        foreach ($data as $value) {
                            echo '<input type="text" class="form-control" name="won_row_3[]" value="' . $value . '"/>';
                        }
                        echo '</div>';

                        echo '<div class="sm-12"><hr/></div>';

                        $data = json_decode($settings['lost_33'], 1);
                        echo '<div class="col-md-4"><h5>Lost <33%</h5>';
                        foreach ($data as $value) {
                            echo '<input type="text" class="form-control" name="lost_33[]" value="' . $value . '"/>';
                        }
                        echo '</div>';

                        $data = json_decode($settings['lost_66'], 1);
                        echo '<div class="col-md-4"><h5>Lost <66%</h5>';
                        foreach ($data as $value) {
                            echo '<input type="text" class="form-control" name="lost_66[]" value="' . $value . '"/>';
                        }
                        echo '</div>';

                        $data = json_decode($settings['lost_100'], 1);
                        echo '<div class="col-md-4"><h5>Lost >66%</h5>';
                        foreach ($data as $value) {
                            echo '<input type="text" class="form-control" name="lost_100[]" value="' . $value . '"/>';
                        }
                        echo '</div>';

                        echo '</div><hr/>
                            <button name="submit_texts_for_game" class="btn btn-lg btn-success">Save</button>
                        </form>';

                        break;

                    case 'submitted_numbers':
                        echo '<h2 class="text-muted">Submitted Numbers (#' . count($all_submitted_numebr) . ')</h2><hr/>';

                        echo '<table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th style="border: 0 !important;">Number</th>
                                    <th style="border: 0 !important;">Name</th>
                                    <th style="border: 0 !important;">Location</th>
                                    <th style="border: 0 !important;">Email</th>
                                    <th style="border: 0 !important;">Link</th>
                                    <th style="border: 0 !important;">Action</th>
                                </tr>
                                </thead>
                            <tbody>';
                        for ($i = 0; $i < sizeof($all_submitted_numebr); $i++) {
                            echo '
                                <tr>
                                    <td style="border: 0 !important;">' . $all_submitted_numebr[$i]['submitted_number'] . '</td>
                                    <td style="border: 0 !important;">' . $all_submitted_numebr[$i]['name'] . '</td>
                                    <td style="border: 0 !important;">' . $all_submitted_numebr[$i]['location'] . '</td>
                                    <td style="border: 0 !important;">' . $all_submitted_numebr[$i]['email'] . '</td>
                                    <td style="border: 0 !important;">' . $all_submitted_numebr[$i]['link'] . '</td>';
                            if ($all_submitted_numebr[$i]['approved'] == 1) {
                                echo '<td style="border: 0 !important;" ><span style="color:green;">Approved</span></td>';
                            } else {
                                echo '<td style="border: 0 !important;" ><a class="approve_submitted_number btn btn-primary" href="joosherinohoneypot.php?approve=' . $all_submitted_numebr[$i]['ID'] . '">Approve</button></td>
                                </tr>';
                            }
                        }
                        echo '</tbody></table>';
                        break;

                    case 'number_queue':
                        echo '<h2 class="text-muted">Number Queue</h2><hr/>';

                        if (isset($_GET['delete'])) {
                            $q = $pdo->prepare('DELETE FROM `number_queue` WHERE `id`=?');
                            $q->execute(array($_GET['delete']));
                        }

                        $q = $pdo->prepare('SELECT * FROM `number_queue` ORDER BY `submitted_at` DESC');
                        $q->execute(array());

                ?>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th style="border: 0 !important;">Number</th>
                                    <th style="border: 0 !important;">Name</th>
                                    <th style="border: 0 !important;">Location</th>
                                    <th style="border: 0 !important;">Email</th>
                                    <th style="border: 0 !important;">Link</th>
                                    <th style="border: 0 !important;">Schedule</th>
                                    <th style="border: 0 !important;">Status</th>
                                    <th style="border: 0 !important;">Date Submitted</th>
                                    <th style="border: 0 !important;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($q as $row) {
                                    echo '
                                        <tr>
                                        <td style="border: 0 !important;">' . $row['number'] . '</td>
                                        <td style="border: 0 !important;">' . $row['name'] . '</td>
                                        <td style="border: 0 !important;">' . $row['location'] . '</td>
                                        <td style="border: 0 !important;">' . $row['email'] . '</td>
                                        <td style="border: 0 !important;">' . $row['link'] . '</td>
                                        <td style="border: 0 !important;">' . date('M jS Y H:i A', $row['queue_time']) . '</td>
                                        <td style="border: 0 !important;">' . ucwords($row['status']) . '</td>
                                        <td style="border: 0 !important;">' . date('M jS Y H:i A', $row['submitted_at']) . '</td>
                                        <td style="border: 0 !important;"><a href="joosherinohoneypot.php?act=number_queue&delete=' . $row['id'] . '" class="btn btn-sm btn-danger">Delete</a></td>
                                        </tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    <?php
                        break;

                    default:
                        echo '<h2 class="text-muted">Numbers for Today</h2>';
                        if (isset($_GET['approve'])) {
                            $update_flag = "UPDATE submittednumbers SET approved=1 WHERE id=" . $_GET['approve'];
                            $pdo->query($update_flag);
                            $requested_user = $pdo->prepare('SELECT * FROM `submittednumbers` WHERE ID = ' . $_GET['approve']);
                            $requested_user->execute();
                            $for_approving_user = $requested_user->fetch(PDO::FETCH_ASSOC);

                            $number_from_db = $for_approving_user['submitted_number'];
                            $name_from_db = $for_approving_user['name'];
                            $location_from_db = $for_approving_user['location'];
                            $email_from_db = $for_approving_user['email'];
                            $link_from_db = $for_approving_user['link'];
                        } else {
                            $number_from_db = '';
                            $name_from_db = '';
                            $location_from_db = '';
                            $email_from_db = '';
                            $link_from_db = '';
                        }
                        if (!empty($_POST['am_number'])) {

                            $q = $pdo->prepare('UPDATE `settings` SET `am_number`=?, `pm_number`=?');
                            $q->execute(array($_POST['am_number'], $_POST['pm_number']));

                            $q = $pdo->prepare('SELECT * FROM `settings` WHERE `id`=1');
                            $q->execute();
                            $settings = $q->fetch(PDO::FETCH_ASSOC);

                            $am_pm = strtolower(date('A'));

                            $q = $pdo->prepare('UPDATE `users` SET `tile_1`=\'\', `tile_2`=\'\', `tile_3`=\'\', `tile_4`=\'\', `tile_5`=\'\', `tile_6`=\'\', `tile_7`=\'\', `tile_8`=\'\', `tile_9`=\'\', `am_pm`=?, `guesses`=\'\', `number12am`=?, `number12pm`=?, `currentRow`=0');
                            $q->execute(array($am_pm, $settings['am_number'], $settings['pm_number']));

                            echo '
                            <div class="alert alert-warning mt-4" role="alert">
                                Successfully changed the numbers for today!
                            </div>';
                        }
                    ?>

                        <form action="" method="post">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="border: 0 !important;">12:00am</th>
                                        <th style="border: 0 !important;">12:00pm</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="border: 0 !important;">
                                            <input type="text" class="form-control" name="am_number" value="<?php echo $settings['am_number']; ?>" />
                                        </td>
                                        <td style="border: 0 !important;">
                                            <input type="text" class="form-control" name="pm_number" value="<?php echo $settings['pm_number']; ?>" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <button class="btn btn-success">Change</button>
                        </form>

                        <hr />
                        <h2 class="text-muted">Queue Number</h2>
                        <?php
                        if (!empty($_POST['q_number'])) {

                            if ($_POST['am_pm'] == 'am') {
                                $ap = '00:00:00';
                            } else {
                                $ap = '12:00:00';
                            }

                            $q = $pdo->prepare('INSERT INTO `number_queue` VALUES (?,?,?,?,?,?,?,?,?)');
                            $q->execute(array(
                                $_POST['q_number'],
                                $_POST['name'],
                                $_POST['location'],
                                $_POST['email'],
                                $_POST['link'],
                                strtotime($_POST['date'] . ' ' . $ap),
                                time(),
                                'active',
                                NULL
                            ));

                            echo '<div class="alert alert-warning mt-4" role="alert">' . $_POST['q_number'] . ' scheduled to run on ' . date('F jS Y H:i A', strtotime($_POST['date'] . ' ' . $ap)) . '</div>';
                        }
                        ?>
                        <form action="" method="post">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="border: 0 !important;">Number</th>
                                        <th style="border: 0 !important;">Name</th>
                                        <th style="border: 0 !important;">Location</th>
                                        <th style="border: 0 !important;">Email</th>
                                        <th style="border: 0 !important;">Link</th>
                                        <th style="border: 0 !important;">Date</th>
                                        <th style="border: 0 !important;">AM/PM</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="border: 0 !important;">
                                            <input type="text" name="q_number" value="<?php echo $number_from_db; ?>" class="form-control" />
                                        </td>
                                        <td style="border: 0 !important;">
                                            <input type="text" name="name" value="<?php echo $name_from_db; ?>" class="form-control" />
                                        </td>
                                        <td style="border: 0 !important;">
                                            <input type="text" name="location" value="<?php echo $location_from_db; ?>" placeholder="Optional" class="form-control" />
                                        </td>
                                        <td style="border: 0 !important;">
                                            <input type="text" name="email" value="<?php echo $email_from_db; ?>" placeholder="Optional" class="form-control" />
                                        </td>
                                        <td style="border: 0 !important;">
                                            <input type="text" name="link" value="<?php echo $link_from_db; ?>" placeholder="Optional" class="form-control" />
                                        </td>
                                        <td style="border: 0 !important;">
                                            <input type="date" name="date" class="form-control" />
                                        </td>
                                        <td style="border: 0 !important;">
                                            <select name="am_pm" class="form-control">
                                                <option value="am">AM</option>
                                                <option value="pm">PM</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <button class="btn btn-success">Submit</button>
                        </form>
                        <hr />
                        <h5>Players this round : #<?= $rounds ?></h5>
                        <h5>Winners this round : #<?= $rounds_won ?></h5>
                <?php
                        break;
                }
                ?>
            </div>
        <?php
        } else {

        ?>
            <form action="" method="post">
                <label>Enter password to continue</label>
                <input type="password" name="password" class="form-control mb-1">
                <button type="submit" class="btn btn-lg btn-success">Enter</button>
            </form>
        <?php
        }
        ?>
    </div>

    </div>

    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            jQuery('.approve_submitted_number').click(function() {
                jQuery('#avitesting').modal('show');
            })

            $.get('_cron.php')
                .done(function(data) {
                    console.log('Cron Sucessfully Executed');
                });
        });
    </script>
</body>

</html>