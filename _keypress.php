<?php
include 'config.php';
$q = $pdo->prepare('SELECT * FROM `users` WHERE `id`=?');
$q->execute(array($_POST['user']));
$data = $q->fetch(PDO::FETCH_ASSOC);

if ($data['guesses'] == '') {
    $q = $pdo->prepare('UPDATE `users` SET `gamesPlayed`=`gamesPlayed`+1 WHERE `id`=?');
    $q->execute(array($data['id']));
}

if (strtolower(date('A')) == 'am') {
    $number_to_guess = str_split($data['number12am']);
    $currentNumber = $data['number12am'];
} else {
    $number_to_guess = str_split($data['number12pm']);
    $currentNumber = $data['number12pm'];
}

if (empty($data['guesses'])) {
    $arr = array();
    $data['guesses'] = json_encode($arr);
}

$guesses = json_decode($data['guesses'], 1);
$guesses[] = $_POST['value_1'];
$guesses[] = $_POST['value_2'];
$guesses[] = $_POST['value_3'];

$submitted_number = $_POST['value_1'] . $_POST['value_2'] . $_POST['value_3'];

function number_classname($number_to_guess, $value, $part, $currentNumber)
{
    if ($_POST[$value] == $number_to_guess[$part]) {
        $className = 'box-green';
    } else {
        $className = 'box-gray';
    }

    if ($className == 'box-green') {
        if (substr_count($currentNumber, $_POST[$value]) > 1) {
            $className = 'box-green-purple';
        }
    }

    if ($className == 'box-gray') {
        if (substr_count($currentNumber, $_POST[$value]) > 0) {
            $className = 'box-yellow';
        }
    }

    if ($className == 'box-yellow') {
        if (substr_count($currentNumber, $_POST[$value]) > 1) {
            $className = 'box-yellow-purple';
        }
    }
    return $className;
}

if ($_POST['currentRow'] == 0) {

    //Tile 1
    $guessPart = '1';
    $className1 = number_classname($number_to_guess, 'value_' . $guessPart, ($guessPart - 1), $currentNumber);

    $array1 = array();
    $array1['value'] = $_POST['value_' . $guessPart];
    $array1['className'] = $className1;
    $array1['animation'] = 'animate__animated animate__flipInX animate__faster';


    //Tile 2
    $guessPart = '2';
    $className2 = number_classname($number_to_guess, 'value_' . $guessPart, ($guessPart - 1), $currentNumber);

    $array2 = array();
    $array2['value'] = $_POST['value_' . $guessPart];
    $array2['className'] = $className2;
    $array2['animation'] = 'animate__animated animate__flipInX animate__faster animate__delay-1s';

    //Tile 3
    $guessPart = '3';
    $className3 = number_classname($number_to_guess, 'value_' . $guessPart, ($guessPart - 1), $currentNumber);

    $array3 = array();
    $array3['value'] = $_POST['value_' . $guessPart];
    $array3['className'] = $className3;
    $array3['animation'] = 'animate__animated animate__flipInX animate__faster animate__delay-2s';

    if ($className1 == $className2) {
        $array1['className'] = str_replace('-purple', '', $className1);
        $array2['className'] = str_replace('-purple', '', $className2);
    }

    if ($className1 == $className3) {
        $array1['className'] = str_replace('-purple', '', $className1);
        $array3['className'] = str_replace('-purple', '', $className3);
    }

    if ($className2 == $className3) {
        $array2['className'] = str_replace('-purple', '', $className2);
        $array3['className'] = str_replace('-purple', '', $className3);
    }

    //Tile 1
    $q = $pdo->prepare('UPDATE `users` SET `tile_1`=?, `guesses`=? WHERE `id`=?');
    $q->execute(array(json_encode($array1), json_encode($guesses), $_POST['user']));

    //Tile 2
    $q = $pdo->prepare('UPDATE `users` SET `tile_2`=?, `guesses`=? WHERE `id`=?');
    $q->execute(array(json_encode($array2), json_encode($guesses), $_POST['user']));

    //Tile 3
    $q = $pdo->prepare('UPDATE `users` SET `tile_3`=?, `guesses`=? WHERE `id`=?');
    $q->execute(array(json_encode($array3), json_encode($guesses), $_POST['user']));
}

if ($_POST['currentRow'] == 1) {

    //Tile 4
    $guessPart = '1';
    $className1 = number_classname($number_to_guess, 'value_' . $guessPart, ($guessPart - 1), $currentNumber);

    $array1 = array();
    $array1['value'] = $_POST['value_' . $guessPart];
    $array1['className'] = $className1;
    $array1['animation'] = 'animate__animated animate__flipInX animate__faster';

    //Tile 5
    $guessPart = '2';
    $className2 = number_classname($number_to_guess, 'value_' . $guessPart, ($guessPart - 1), $currentNumber);

    $array2 = array();
    $array2['value'] = $_POST['value_' . $guessPart];
    $array2['className'] = $className2;
    $array2['animation'] = 'animate__animated animate__flipInX animate__faster animate__delay-1s';

    //Tile 6
    $guessPart = '3';
    $className3 = number_classname($number_to_guess, 'value_' . $guessPart, ($guessPart - 1), $currentNumber);

    $array3 = array();
    $array3['value'] = $_POST['value_' . $guessPart];
    $array3['className'] = $className3;
    $array3['animation'] = 'animate__animated animate__flipInX animate__faster animate__delay-2s';

    if ($className1 == $className2) {
        $array1['className'] = str_replace('-purple', '', $className1);
        $array2['className'] = str_replace('-purple', '', $className2);
    }

    if ($className1 == $className3) {
        $array1['className'] = str_replace('-purple', '', $className1);
        $array3['className'] = str_replace('-purple', '', $className3);
    }

    if ($className2 == $className3) {
        $array2['className'] = str_replace('-purple', '', $className2);
        $array3['className'] = str_replace('-purple', '', $className3);
    }

    //Tile 4
    $q = $pdo->prepare('UPDATE `users` SET `tile_4`=?, `guesses`=? WHERE `id`=?');
    $q->execute(array(json_encode($array1), json_encode($guesses), $_POST['user']));

    //Tile 5
    $q = $pdo->prepare('UPDATE `users` SET `tile_5`=?, `guesses`=? WHERE `id`=?');
    $q->execute(array(json_encode($array2), json_encode($guesses), $_POST['user']));

    //Tile 6
    $q = $pdo->prepare('UPDATE `users` SET `tile_6`=?, `guesses`=? WHERE `id`=?');
    $q->execute(array(json_encode($array3), json_encode($guesses), $_POST['user']));
}

if ($_POST['currentRow'] == 2) {

    //Tile 7
    $guessPart = '1';
    $className1 = number_classname($number_to_guess, 'value_' . $guessPart, ($guessPart - 1), $currentNumber);

    $array1 = array();
    $array1['value'] = $_POST['value_' . $guessPart];
    $array1['className'] = $className1;
    $array1['animation'] = 'animate__animated animate__flipInX animate__faster';

    //Tile 8
    $guessPart = '2';
    $className2 = number_classname($number_to_guess, 'value_' . $guessPart, ($guessPart - 1), $currentNumber);

    $array2 = array();
    $array2['value'] = $_POST['value_' . $guessPart];
    $array2['className'] = $className2;
    $array2['animation'] = 'animate__animated animate__flipInX animate__faster animate__delay-1s';

    //Tile 9
    $guessPart = '3';
    $className3 = number_classname($number_to_guess, 'value_' . $guessPart, ($guessPart - 1), $currentNumber);

    $array3 = array();
    $array3['value'] = $_POST['value_' . $guessPart];
    $array3['className'] = $className3;
    $array3['animation'] = 'animate__animated animate__flipInX animate__faster animate__delay-2s';

    if ($className1 == $className2) {
        $array1['className'] = str_replace('-purple', '', $className1);
        $array2['className'] = str_replace('-purple', '', $className2);
    }

    if ($className1 == $className3) {
        $array1['className'] = str_replace('-purple', '', $className1);
        $array3['className'] = str_replace('-purple', '', $className3);
    }

    if ($className2 == $className3) {
        $array2['className'] = str_replace('-purple', '', $className2);
        $array3['className'] = str_replace('-purple', '', $className3);
    }

    //Tile 7
    $q = $pdo->prepare('UPDATE `users` SET `tile_7`=?, `guesses`=? WHERE `id`=?');
    $q->execute(array(json_encode($array1), json_encode($guesses), $_POST['user']));

    //Tile 8
    $q = $pdo->prepare('UPDATE `users` SET `tile_8`=?, `guesses`=? WHERE `id`=?');
    $q->execute(array(json_encode($array2), json_encode($guesses), $_POST['user']));

    //Tile 9
    $q = $pdo->prepare('UPDATE `users` SET `tile_9`=?, `guesses`=? WHERE `id`=?');
    $q->execute(array(json_encode($array3), json_encode($guesses), $_POST['user']));
}

$q = $pdo->prepare('UPDATE `users` SET `currentRow`=`currentRow`+1 WHERE `id`=?');
$q->execute(array($_POST['user']));
