<?php
include 'config.php';

if (isset($_POST['image'])) {
    $output_dir = "share/";

    $data = $_POST['image'];

    $image_array_1 = explode(";", $data);

    $image_array_2 = explode(",", $image_array_1[1]);

    $data = base64_decode($image_array_2[1]);

    $file_name =  'IMG_' . time() . '.png';
    $image_name = $output_dir . $file_name;

    $return_url = $root . '/' . $output_dir . $file_name;

    file_put_contents($image_name, $data);
    echo $return_url;
}
