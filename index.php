<?php
include 'config.php';

$am_pm = strtolower(date('A'));

$q = $pdo->prepare('SELECT * FROM `settings` WHERE `id`=1');
$q->execute();
$settings = $q->fetch(PDO::FETCH_ASSOC);

$q = $pdo->prepare('SELECT * FROM `users` WHERE `IP`=?');
$q->execute(array($_SERVER['REMOTE_ADDR']));

if ($q->rowcount() < 1) {
    $q = $pdo->prepare('INSERT INTO `users` VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
    $q->execute(['', $_SERVER['REMOTE_ADDR'], '', 0, 0, '', 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', $am_pm, $settings['am_number'], $settings['pm_number'], NULL]);

    unset($_SESSION['statistics_title']);
    $_SESSION['showHelp'] = 1;
    $_SESSION['wonGame'] = 1;
    header('Location: /');
    exit();
}

$data = $q->fetch(PDO::FETCH_ASSOC);

if ($data['am_pm'] != $am_pm) {
    $q = $pdo->prepare('UPDATE `users` SET `tile_1`=\'\', `tile_2`=\'\', `tile_3`=\'\', `tile_4`=\'\', `tile_5`=\'\', `tile_6`=\'\', `tile_7`=\'\', `tile_8`=\'\', `tile_9`=\'\', `am_pm`=?, `guesses`=\'\', `number12am`=?, `number12pm`=?, `currentRow`=0 WHERE `id`=?');
    $q->execute(array($am_pm, $settings['am_number'], $settings['pm_number'], $data['id']));
    unset($_SESSION['submitAnswer']);
    unset($_SESSION['statistics_title']);
    header('Location: /');
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property=“og:image” content="img/rockf.png" />
    <meta property=”og:image:width” content=”180″ />
    <meta property=”og:image:height” content=”110″ />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="https://guessmynumbers.com/" />
    <meta name="twitter:title" content="Guess my number" />
    <meta name="twitter:description" content="Guess my number - A daily number game" />
    <meta name="twitter:image" content="https://guessmynumbers.com/img/rockf.png" />
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    <title>Guess My Number - A Daily Number Game</title>
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lora:wght@600&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script src='https://cdn.jsdelivr.net/npm/canvas-confetti@1.2.0/dist/confetti.browser.min.js'></script>
    <style type="text/css">
        .color-ful {
            color: purple;
        }

        .modal-dialog {
            max-width: 400px !important;
        }

        .color-ful a {
            color: #198754;
            text-decoration: none;
        }

        #statistics {
            transition: 1.5s;
            transform-style: preserve-3d;
        }

        #all_statistics>table {
            margin-bottom: 10px;
        }

        #show_pic_modal {
            transition: 0.5s;
            transform-style: preserve-3d;
        }

        #gameData2 .content {
            display: none !important;
        }

        #gameData2 .box {
            width: 100%;
            margin: 0 auto;
        }

        #statistics>div>div>div>canvas {
            margin-left: 10%;
        }

        .my-numb {
            color: #198754;
        }

        img {
            max-width: 100%;
        }

        i.far,
        i.fas {
            color: #777;
            cursor: pointer;
        }

        i.far:hover,
        i.fas:hover {
            color: #777;
        }

        #gameData {
            position: relative;
            /* left: 18px; */
        }

        .styling_box_for_number {
            width: 20%;
        }

        .box {
            position: relative;
            width: 65%;
            font-size: clamp(2rem, 10vw, 5rem);
            text-align: center;
            font-weight: bold;
        }

        .box:after {
            content: "";
            display: block;
            padding-bottom: 100%;
        }

        .content {
            position: absolute;
            top: 0px;
            width: 100%;
            height: 100%;
        }

        .box-white {
            background: #EFEFEF;
            color: #555;
        }

        .box-purple {
            background: #5C05C8;
            color: #FFF;
        }

        .box-gray {
            background: #6e6e6e;
            color: #FFF;
        }

        .box-green {
            background: #4c9e3a;
            color: #FFF;
        }

        .box-green-purple {
            background-image: url('img/right-bgxx.png');
            background-size: 100% 100%;
            color: #FFF;
        }

        .box-yellow {
            background: #9a8f02;
            color: #FFF;
        }

        .box-yellow-purple {
            background-image: url('img/yellow-purple.png');
            background-size: 100% 100%;
            color: #FFF;
        }

        .circle-div {
            margin: 0 auto;
            text-align: center;
            height: 50px;
            width: 50px;
            border-radius: 100%;
            background: #EFEEE9;
            text-align: center;
            color: #555;
            font-weight: bold;
            font-size: xx-large;
            padding-top: 0px;
            cursor: pointer;
        }

        .circle-div-active {
            margin: 0 auto;
            text-align: center;
            height: 50px;
            width: 50px;
            border-radius: 100%;
            text-align: center;
            font-weight: bold;
            font-size: xx-large;
            padding-top: 0px;
            cursor: pointer;
        }

        .lora {
            font-family: 'Lora', serif;
        }

        #row_1 td div,
        #row_2 td div,
        #row_3 td div {
            width: 100%;
        }

        #numpad table {
            margin-left: 18px;
        }

        div#all_statistics table.table h1 {
            font-size: 24px;
        }

        div#gameData2 {
            padding: 0 20px;
        }

        @media only screen and (max-width: 700px) {
            body {
                overflow-x: hidden !important;
                overflow-y: hidden !important;
            }

            #statistics>div>div>div>canvas {
                margin-left: unset !important;
            }

            .circle-div,
            .circle-div-active {
                height: 50px;
                width: 50px;
                padding-top: 12px;
                font-size: medium;
                cursor: pointer;
            }
        }

        #gameData #row_1 td div,
        #gameData #row_2 td div,
        #gameData #row_3 td div {

            width: 100%;
            height: 130px;
        }

        #numpad table {
            margin-left: 0;
        }

        [attr-numpad] {
            cursor: pointer;
        }

        [attr-numpad]:hover {
            opacity: 0.75;
        }

        .numpad-disabled {
            opacity: 0.5;
        }

        .modal-backdrop {
            opacity: 0.5 !important;
        }

        .modal-content {
            -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.73);
            -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.73);
            box-shadow: 0 3px 7px rgba(0, 0, 0, 0.73);
        }

        .modal-backdrop {
            display: none;
        }

        @media only screen and (max-width: 500px) {
            .sumbit-number-box {
                max-height: 90.4vh;
                height: 646px;
            }

            #submit_numbers {
                font-size: 14px;
            }

            .content {
                display: flex !important;
                justify-content: center;
                align-items: center;
            }

            #gameData #row_1 td div,
            #gameData #row_2 td div,
            #gameData #row_3 td div {
                height: 110px;
                width: 100% !important;
                display: flex !important;
                justify-content: center;
                align-items: center;
            }

            #firstdiv {
                margin: 1em 0em 0.5em 0em !important;
            }

            #dateid h3 {
                font-size: 20px;
            }

            #dateid p {
                font-size: 15px;
            }

            #gameData {
                left: 0px !important;
                top: 0px !important;
            }

            #numpad {
                margin-top: 0px !important;
            }

            h4 {
                font-size: calc(1.4rem + .3vw) !important;
            }

            .h3,
            h3 {
                font-size: calc(1rem + .6vw);
            }

            .modal-body {
                padding: 1rem;
            }

            #all_statistics {
                font-size: 13px;
                margin-top: 0;
            }

            .guessid {

                font-size: x-large;
                margin-top: 0px !important;
                margin-bottom: 0px !important;
            }

            #statistics_title {
                font-size: 36px !important;
                padding: 0 20px;
            }

            #gameData2 {
                margin: 0 !important;
                padding: 0 20px;
            }

        }

        @media only screen and (width: 320px) {
            .d-flex-mob320 {
                display: flex;
            }

            .circle-div,
            .circle-div-active {
                height: 44px;
                width: 44px;
            }

            #gameData #row_1 td div,
            #gameData #row_2 td div,
            #gameData #row_3 td div {
                height: 87px;
            }
        }

        @media only screen and (min-width: 1200px) {
            .sumbit-number-box {
                min-height: 520px;
                height: 100%;
            }
        }

        #dateid {
            width: 100%;
        }

        #statistics::-webkit-scrollbar {
            display: none;
        }

        .rock-flex>img {
            max-height: 480px;
            left: 0;
            padding: 0;
            margin: 0 auto;
            display: block;
        }

        div#all_statistics table.table h1 {
            line-height: 5px;
        }
    </style>
    <?php
    if ($data['darkTheme'] == 1) {
    ?>
        <style type="text/css">
            body {
                background: #333;
            }

            h1,
            h2,
            h3,
            h4,
            h5,
            h6,
            p,
            td {
                color: #FFF !important;
            }

            i.far,
            i.fas {
                color: #FFF;
                cursor: pointer;
            }

            i.far:hover,
            i.fas:hover {
                color: #777;
            }

            .modal-content {
                background: #333;
                color: #FFF !important;
            }

            .modal-content a {
                color: #EFEFEF !important;
            }

            .text-muted {
                color: #EFEFEF !important;
            }

            .box-white {
                background: #888888;
                color: #FFF;
            }

            @media only screen and (max-width: 500px) {
                .modal-body {
                    padding: 2rem 0.5rem !important;
                }
            }
        </style>
    <?php
    }
    ?>
    <?php
    if ($data['colorBlind'] == 1) {
    ?>
        <style type="text/css">
            .box-green {
                background: orange;
            }

            .box-green-purple {
                background: orange;
            }

            .box-yellow {
                background: lightblue;
            }

            .box-yellow-purple {
                background: lightblue;
            }
        </style>
    <?php
    }
    ?>
</head>

<body>
    <?php

    if (strtolower(date('A')) == 'am') {
        $am = 1;
        $number = $data['number12am'];
        $next_num_timestamp = strtotime("today 12:00:00");
    } else {
        $pm = 1;
        $number = $data['number12pm'];
        $next_num_timestamp = strtotime("tomorrow 00:00:00");
    }

    if (isset($_POST['settings'])) {

        if (isset($_POST['darkMode'])) {
            $q = $pdo->prepare('UPDATE `users` SET `darkTheme`=1 WHERE `id`=?');
            $q->execute(array($data['id']));
        } else {
            $q = $pdo->prepare('UPDATE `users` SET `darkTheme`=0 WHERE `id`=?');
            $q->execute(array($data['id']));
        }

        if (isset($_POST['colorBlind'])) {
            $q = $pdo->prepare('UPDATE `users` SET `colorBlind`=1 WHERE `id`=?');
            $q->execute(array($data['id']));
        } else {
            $q = $pdo->prepare('UPDATE `users` SET `colorBlind`=0 WHERE `id`=?');
            $q->execute(array($data['id']));
        }

        $_SESSION['settings'] = 1;
        header('Location: /');
        exit();
    }

    ?>

    <center>
        <div style="display:inline-flex; margin-top: 10px;" id="firstdiv">

            <div style="float: left;">
                <i class="far fa-question-circle" id="show_help" style="font-size: 24px; margin-left: 10px;"></i>
            </div>
            <div style="text-align:center; padding:0 20px;">
                <h4 style="line-height: .9">GUESS <span class="my-numb">MY</span> NUMBERS</h4>
            </div>
            <div style="float: right;" class="d-flex-mob320">
                <i class="far fa-chart-bar" id="show_statistics" style="font-size: 24px; margin-right: 8px;"></i> <i class="fas fa-cog" id="show_settings" style="font-size: 24px; margin-right: 8px;"></i>
            </div>
            <div class="clearfix"></div>

        </div>
    </center>

    <div style="max-width: 400px; margin: 0 auto; padding: 0px;" class="pageContent">

        <div class="row" id="dateid">
            <div class="col-sm-12">
                <?php
                if (!empty($settings['queue_data'])) {
                    $current_date = strtotime(date('m/d/Y'));

                    $by = json_decode($settings['queue_data'], 1);
                    echo '<h3 style="text-align:left; margin-left:20px;">' . date('F jS Y', $current_date) . '</h3>';
                    echo '<p class="text-muted" style="margin-left: 20px;">Submitted by:<span class="color-ful"><a href="' . ($by['link'] ? 'https://' . $by['link'] : 'javascript:void(0)') . '" target="_blank"> ' . $by['name'] . ', ' . $by['location'] . '</a></span> <i class="fas fa-check-circle text-success"></i></p>';
                }

                if (isset($_POST['submiting_number_button'])) {
                    $submittednumber = '';
                    $nameforsubmitting = '';
                    $locationforsubmitting = '';
                    $emailforsubmitting = '';
                    $linkforsubmitting = '';

                    if (isset($_POST['submittednumber'])) {
                        $submittednumber = $_POST['submittednumber'];
                    }
                    if (isset($_POST['nameforsubmittingnumbers'])) {
                        $nameforsubmitting = $_POST['nameforsubmittingnumbers'];
                    }
                    if (isset($_POST['locationforsubmittingnumbers'])) {
                        $locationforsubmitting = $_POST['locationforsubmittingnumbers'];
                    }
                    if (isset($_POST['emailforsubmittingnumbers'])) {
                        $emailforsubmitting = $_POST['emailforsubmittingnumbers'];
                    }
                    if (isset($_POST['linkforsubmittingnumbers'])) {
                        $linkforsubmitting = $_POST['linkforsubmittingnumbers'];
                    }

                    $_SESSION['submitAnswer'] = 1;
                    $insert_submitted_number = "INSERT INTO submittednumbers (submitted_number , name, location, email, link) VALUES (" . $submittednumber . ", '" . $nameforsubmitting . "', '" . $locationforsubmitting . "', '" . $emailforsubmitting . "', '" . $linkforsubmitting . "')";
                    $pdo->query($insert_submitted_number);
                    header('Location: /');
                }
                ?>
            </div>
        </div>

        <div id="gameData">
            <table style="width: 100%; border-spacing: 5px; border-collapse: separate;">
                <tbody>
                    <tr id="row_1">
                        <td>
                            <div class="box box-white">
                                <div class="content"><span style="opacity: 0;">0</span></div>
                            </div>
                        </td>
                        <td>
                            <div class="box box-white">
                                <div class="content"><span style="opacity: 0;">0</span></div>
                            </div>
                        </td>
                        <td>
                            <div class="box box-white">
                                <div class="content"><span style="opacity: 0;">0</span></div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table style="width: 100%; border-spacing: 5px; border-collapse: separate;">
                <tbody>
                    <tr id="row_2">
                        <td>
                            <div class="box box-white">
                                <div class="content"><span style="opacity: 0;">0</span></div>
                            </div>
                        </td>
                        <td>
                            <div class="box box-white">
                                <div class="content"><span style="opacity: 0;">0</span></div>
                            </div>
                        </td>
                        <td>
                            <div class="box box-white">
                                <div class="content"><span style="opacity: 0;">0</span></div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table style="width: 100%; border-spacing: 5px; border-collapse: separate;">
                <tbody>
                    <tr id="row_3">
                        <td>
                            <div class="box box-white">
                                <div class="content"><span style="opacity: 0;">0</span></div>
                            </div>
                        </td>
                        <td>
                            <div class="box box-white">
                                <div class="content"><span style="opacity: 0;">0</span></div>
                            </div>
                        </td>
                        <td>
                            <div class="box box-white">
                                <div class="content"><span style="opacity: 0;">0</span></div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <br>

        <div id="numpad">
            <table style="width: 100%; border-spacing: 5px; border-collapse: separate;">
                <tr>
                    <td>
                        <div class="circle-div" attr-numpad="0">
                            0
                        </div>
                    </td>
                    <td>
                        <div class="circle-div" attr-numpad="1">
                            1
                        </div>
                    </td>
                    <td>
                        <div class="circle-div" attr-numpad="2">
                            2
                        </div>
                    </td>
                    <td>
                        <div class="circle-div" attr-numpad="3">
                            3
                        </div>
                    </td>
                    <td>
                        <div class="circle-div" attr-numpad="4">
                            4
                        </div>
                    </td>
                    <td style="width: 70px;">
                        <div class="circle-div" attr-numpad="back">
                            <img src="img/back.png" style="width: 95%; margin-top: -5px">
                        </div>
                    </td>
                </tr>
            </table>

            <table style="width: 100%; border-spacing: 5px; border-collapse: separate;">
                <tr>
                    <td>
                        <div class="circle-div" attr-numpad="5">
                            5
                        </div>
                    </td>
                    <td>
                        <div class="circle-div" attr-numpad="6">
                            6
                        </div>
                    </td>
                    <td>
                        <div class="circle-div" attr-numpad="7">
                            7
                        </div>
                    </td>
                    <td>
                        <div class="circle-div" attr-numpad="8">
                            8
                        </div>
                    </td>
                    <td>
                        <div class="circle-div" attr-numpad="9">
                            9
                        </div>
                    </td>
                    <td style="width: 70px;">
                        <div class="circle-div" attr-numpad="enter">
                            <img src="img/enter.png" style="width: 95%; margin-top: -9px">
                        </div>
                    </td>
                </tr>
            </table>
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade animate__animated" id="statistics" tabindex="-1" role="dialog" aria-labelledby="statisticsLabel" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
        <div class="modal-dialog" role="document" style="max-width: 500px;">
            <div class="modal-content">
                <div class="modal-body">
                    <div style="position: absolute; top:2px; right:7px;">
                        <button type="button" class="close" onclick="closeModal()" style="border: none !important; background: none;" data-dismiss="statistics" aria-label="Close">
                            <span aria-hidden="true" style="font-weight: bold; font-size: x-large;">&times;</span>
                        </button>
                    </div>
                    <div class="clearfix"></div>
                    <h1 style="font-size: 36px; text-align: center;" id="statistics_title">STATISTICS</h1>
                    <div id="all_statistics"></div>
                    <p style="text-align: center; font-size: x-large; margin-bottom:0px;" class="guessid">YOUR GUESSES</p>
                    <div id="guesses_data" style="visibility: hidden !important;"></div>
                    <table class="table" style="margin-bottom: 0;">
                        <tr>
                            <td style="text-align: center; width: 50%; vertical-align: middle;">
                                NEW NUMBER IN:<br />
                                <h1 id="time_until">00:00:00</h1>
                            </td>
                            <td style="text-align: center; width: 50%; vertical-align: middle;">
                                <button class="btn btn-success" id="share-num" onclick="shareNum()" style="border: none !important; display: block; width: 100%; margin-bottom: 10px;">SHARE</button>
                                <?php if (!isset($_SESSION['submitAnswer'])) { ?>
                                    <button class="btn btn-warning" id="submit_numbers" onclick="submit_numbers()" style="display: none; background-color: purple !important;">SUBMIT NUMBERS</button>
                                <?php } ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="text-center">
                    <p>www.guessmynumbers.com</p>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade animate__animated" style="margin-top: 4%; " id="show_pic_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 500px;">
            <div class="modal-content sumbit-number-box">
                <div class="modal-body">
                    <div class="rock-flex"><img src="img/rockf.png" style="max-width: 100%; padding-bottom: 20%; margin-top: 10%;"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="submit_numbers_modal" style="margin-top: 4%;" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 500px;">
            <div class="modal-content sumbit-number-box">
                <div class="modal-body">
                    <div style="position: absolute; top:2px; right:7px;">
                        <button type="button" class="btn-close" onclick="closeSubmit()" data-bs-dismiss="modal" aria-label="Close" style="font-size:12px;"></button>
                    </div>
                    <div class="clearfix"></div>
                    <h1 style="font-size: 25px; text-align: center; color: green; margin-bottom: 15px">VIP SECTION</h1>
                    <div class="clearfix"></div>
                    <form action="" method="post">
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="submittednumber" placeholder="Your THREE Numbers*" pattern="[0-9]+" maxlength="3" title="Please Enter Only Numbers" required="required">
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col">
                                <input type="text" name="nameforsubmittingnumbers" class="form-control" placeholder="Name*" required="required">
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col">
                                <input type="text" name="locationforsubmittingnumbers" class="form-control" placeholder="Location* (Country or State)" required="required">
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col">
                                <input type="email" name="emailforsubmittingnumbers" class="form-control" placeholder="Email (optional)">
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col">
                                <input type="text" name="linkforsubmittingnumbers" class="form-control" placeholder="Website/Social Media Link (optional)">
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col text-center">
                                <input type="submit" style="background-color: purple;color: white;border: none;" class="btn btn-success" value="SUBMIT YOUR NUMBERS" name="submiting_number_button">
                            </div>
                        </div>

                    </form>
                </div>
                <div class="text-center">
                    <a href="how_to_play.php"><i>How it works</i></a>
                    <p>www.guessmynumbers.com</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="help" tabindex="-1" role="dialog" aria-labelledby="helpLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 500px;">
            <div class="modal-content">
                <div class="modal-body">
                    <div style="float: right;">
                        <button type="button" class="close" style="border: none !important; background: #FFF;" data-dismiss="help" aria-label="Close">
                            <span aria-hidden="true" style="font-weight: bold; font-size: x-large;">&times;</span>
                        </button>
                    </div>
                    <div class="clearfix"></div>
                    <h1 style="margin: 0 auto; text-align: center; margin-top: -40px;">
                        <img src="img/logo.jpeg" style="width: 120px;" />
                    </h1>
                    <h4>How to Play</h4>
                    <p style="color: #333;">
                        You have three tries to guess the featured number.<br />
                        Press the check mark to submit. <img src="img/enter.png" style="width: 40px;"><br /><br />
                        After each guess, the colors will change to show how close you got!<br /><br />
                        If you guess correctly, <b>you have a chance to submit your own numbers to be featured!</b><br /><br />
                        <img src="img/help_green.PNG" style="height: 70px;"><br />
                        <strong>Green</strong> indicates the number is in the proper spot.<br /><br />
                        <img src="img/help_yellow.PNG" style="height: 70px;"><br />
                        <strong>Yellow</strong> indicates the number is correct, but in the wrong spot.<br /><br />
                        <img src="img/help_double.PNG" style="height: 70px;"><br />
                        A <strong>Purple</strong> line indicates the number is used multiple times.
                    </p>
                    <hr />
                    <p class="text-center">
                        <strong>New Numbers updated every 12 hours!</strong><br />
                        <span class="text-muted">12:00am PST / 12:00pm PST</span>
                    </p>
                </div>
                <div class="text-center">
                    <p>www.guessmynumbers.com</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="settingsLabel" aria-hidden="true">
        <div class="modal-dialog " role="document" style="max-width: 500px;">
            <div class="modal-content">
                <div class="modal-body">
                    <div style="float: right;">
                        <button type="button" class="close" style="border: none !important; background: #FFF;" data-dismiss="settings" aria-label="Close">
                            <span aria-hidden="true" style="font-weight: bold; font-size: x-large;">&times;</span>
                        </button>
                    </div>
                    <div class="clearfix"></div>
                    <h1 style="margin: 0 auto; text-align: center; margin-top: -40px;">
                        Settings
                    </h1>
                    <form action="" method="post" style="margin-top: 20px;" id="settingsForm">

                        <!-- <div style=" padding-top: 10px;">
                            <div style="float: left;">
                                <p style="color: #333; font-weight: bold; font-size: x-large;">Dark Mode</p>
                            </div>
                            <div style="float: right;">
                                <div class="form-check form-switch" style="transform: scale(1.5); margin-top: 5px;">
                                    <input class="form-check-input" type="checkbox" role="switch" name="darkMode" <?php if ($data['darkTheme'] == 1) {
                                                                                                                        echo ' checked';
                                                                                                                    } ?>>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div> -->

                        <div style=" padding-top: 10px;">
                            <div style="float: left;">
                                <p style="color: #333; font-weight: bold; font-size: x-large;">Practice Mode</p>
                            </div>
                            <div style="float: right;">
                                <div>
                                    Coming Soon
                                </div>
                                <!-- <div class="form-check form-switch" style="transform: scale(1.5); margin-top: 5px;">
                                    <input class="form-check-input" type="checkbox" role="switch" name="darkMode" <?php if ($data['darkTheme'] == 1) {
                                                                                                                        echo ' checked';
                                                                                                                    } ?>>
                                </div> -->
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div style=" padding-top: 10px;">
                            <div style="float: left;">
                                <p style="color: #333; font-weight: bold; font-size: x-large;">High Contrast<br />
                                    <small class="text-muted" style="font-size: small; font-weight: normal;">For color adjustments</small>
                                </p>
                            </div>
                            <div style="float: right;">
                                <div class="form-check form-switch" style="transform: scale(1.5); margin-top: 5px;">
                                    <input class="form-check-input" type="checkbox" role="switch" name="colorBlind" <?php if ($data['colorBlind'] == 1) {
                                                                                                                        echo ' checked';
                                                                                                                    } ?>>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div style=" padding-top: 10px;">
                            <div style="float: left;">
                                <p style="color: #333; font-weight: bold; font-size: x-large;">Feedback</p>
                            </div>
                            <div style="float: right;">
                                <a href="mailto:josh@guessmynumbers.com" target="_blank" style="font-size: x-large; color: #555;">Email</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div style=" padding-top: 10px;">
                            <div style="float: left;">
                                <p style="color: #333; font-weight: bold; font-size: x-large;">Community</p>
                            </div>
                            <div style="float: right;">
                                <a href="https://twitter.com/guessmynumbers" target="_blank" style="font-size: x-large; color: #555;">Twitter</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div style=" padding-top: 10px; padding-bottom: 10px;">
                            <div style="float: left;">
                                Looking for a great book?
                            </div>
                            <div style="float: right; text-align: right; font-size: small;">
                                <a href="https://amzn.to/3GP2yt4" target="_blank">The Adventures of<br />Rockford T.Honeypot</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <br />
                        <input type="hidden" name="settings" value="1" />
                        <div class="text-center"> Created by<a href="https://twitter.com/joshgottsegen" target="_blank" style="text-decoration: none;"> @JoshGottsegen</a><br /> www.guessmynumbers.com</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Celebration Effect -->
    <div class="js-confetti celebration-btn"></div>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/celebration-script.js"></script>
    <script type="text/javascript">
        var isPlay = 1;
        $(document).ready(function() {
            <?php
            if (!empty($_SESSION['settings'])) {
            ?> setTimeout(function() {
                    $('#settings').modal('show');
                }, 250);
            <?php
                unset($_SESSION['settings']);
            }
            ?>

            $('[name="darkMode"], [name="colorBlind"]').change(function() {
                $('#settingsForm').submit();
            });

            <?php
            if (isset($_SESSION['showHelp'])) {
            ?> setTimeout(function() {
                    $('#help').modal('show');
                }, 100);
            <?php
                unset($_SESSION['showHelp']);
            }
            ?>

            setInterval(function() {
                $.get('_time_until.php', {
                        timestamp: '<?php echo $next_num_timestamp; ?>'
                    })
                    .done(function(data) {
                        var data = JSON.parse(data);

                        if (data.h == 00 && data.m == 00 && data.s == 00) {
                            location.reload();
                        }

                        $('#time_until').text(data.h + ':' + data.m + ':' + data.s);
                    });
            }, 500)

            function stylize_numpad() {
                $('body').find('td div').each(function() {
                    if ($(this).hasClass('box')) {
                        var number = $(this).text().trim();
                        var className = $(this).attr('class').replace('box ', '');
                        $('[attr-numpad="' + number + '"]').removeClass('circle-div').removeClass('box-yellow').removeClass('box-green').removeClass('box-yellow-purple').removeClass('box-green-purple').addClass('circle-div-active').addClass(className);
                    }
                });
            }

            var currentRow = <?php echo $data['currentRow']; ?>;
            var user = <?php echo $data['id']; ?>;
            var rowTileNumber = 1;

            function show_statistics() {

                var correct_row = 0;
                var correct = 0;
                var wrong = 0;
                var rows_data = [];

                $.post('_all_statistics.php', {
                        user: user
                    })
                    .done(function(data) {
                        $('#all_statistics').html(data);
                    });

                $('#row_1').find('td div').each(function() {
                    var className = $(this).attr('class');
                    if (className.indexOf('green') > 0) {
                        correct++;
                    } else {
                        wrong++;
                    }
                    if (correct == 3) {
                        correct_row = 1;
                    }
                    rows_data[1] = correct;
                });

                correct = 0;
                $('#row_2').find('td div').each(function() {
                    var className = $(this).attr('class');
                    if (className.indexOf('green') > 0) {
                        correct++;
                    } else {
                        wrong++;
                    }
                    if (correct == 3) {
                        correct_row = 2;
                    }
                    rows_data[2] = correct;
                });

                correct = 0;
                $('#row_3').find('td div').each(function() {
                    var className = $(this).attr('class');
                    if (className.indexOf('green') > 0) {
                        correct++;
                    } else {
                        wrong++;
                    }
                    if (correct == 3) {
                        correct_row = 3;
                    }
                    rows_data[3] = correct;
                });

                $.get('_statistics_info.php', {
                        total_wrong: wrong,
                        correct_row: correct_row
                    })
                    .done(function(data) {

                        $('#guesses_data').html('');

                        $.each(rows_data, function(index, value) {
                            if (value == 0) {
                                $('#guesses_data').append('<div class="box-gray" style="padding: 5px; font-size: small; margin-bottom: 10px; text-align: left; width: 10% !important;">0</div>');
                            }
                            if (value == 1) {
                                $('#guesses_data').append('<div class="box-yellow" style="padding: 5px; font-size: small; margin-bottom: 10px; text-align: left; width: 33% !important;">1</div>');
                            }
                            if (value == 2) {
                                $('#guesses_data').append('<div class="box-yellow" style="padding: 5px; font-size: small; margin-bottom: 10px; text-align: left; width: 66% !important;">2</div>');
                            }
                            if (value == 3) {
                                $('#guesses_data').append('<div class="box-green" style="padding: 5px; font-size: small; margin-bottom: 10px; text-align: left; width: 100% !important;">3</div>');
                            }
                        });

                        $('#statistics_title').text(data);
                        if (currentRow < 3) {
                            $('#statistics_title').text('Statistics');
                        }
                        $('#statistics').modal('show');
                    });

                gameEndCard();
            }

            $('#show_statistics').click(function() {
                show_statistics();
            });

            $('#show_help').click(function() {
                $('#help').modal('show');
            });

            $('#show_settings').click(function() {
                $('#settings').modal('show');
            });

            $('[attr-numpad]').click(function() {
                if (isPlay == 1) {
                    var key = $(this).attr('attr-numpad');

                    if (key != 'back' && key != 'enter') {

                        if (rowTileNumber < 4) {
                            $('#row_' + (currentRow + 1)).find('td:nth-child(' + rowTileNumber + ')').html('<div class="box box-white animate__animated animate__pulse animate__faster"><span class="content" style="display: block;">' + key + '</span></div>');
                            rowTileNumber++;
                        } else {
                            $('[attr-numpad="enter"]').addClass('animate__animated animate__shakeY');
                            setTimeout(function() {
                                $('[attr-numpad="enter"]').removeClass('animate__animated');
                                $('[attr-numpad="enter"]').removeClass('animate__pulse');
                            }, 1000);
                        }

                    }

                    if (key == 'back') {
                        rowTileNumber--;
                        $('#row_' + (currentRow + 1)).find('td:nth-child(' + rowTileNumber + ')').html('<div class="box box-white"><div class="content"><span style="opacity: 0;">0</span></div></div');
                    }

                    if (key == 'enter') {
                        if (currentRow < 3) {
                            var value1 = $('#row_' + (currentRow + 1)).find('td:nth-child(1) div').text();
                            var value2 = $('#row_' + (currentRow + 1)).find('td:nth-child(2) div').text();
                            var value3 = $('#row_' + (currentRow + 1)).find('td:nth-child(3) div').text();
                            if (value1 == '00' || value2 == '00' || value3 == '00') {
                                alert('Please fill the remaining empty sqaure(s) to guess the number');
                            } else {
                                $.post('_keypress.php', {
                                        user: user,
                                        currentRow: currentRow,
                                        value_1: $('#row_' + (currentRow + 1)).find('td:nth-child(1) div').text(),
                                        value_2: $('#row_' + (currentRow + 1)).find('td:nth-child(2) div').text(),
                                        value_3: $('#row_' + (currentRow + 1)).find('td:nth-child(3) div').text()
                                    })
                                    .done(function() {

                                        $.get('gameData.php', {
                                                user: user,
                                                currentRow: currentRow
                                            })
                                            .done(function(data) {

                                                var res = JSON.parse(data);
                                                if (res.won) {
                                                    isPlay = 0;
                                                    $('#gameData').html(res.content);
                                                    $('#guesses_data').html(res.content);

                                                    setTimeout(function() {
                                                        $('#submit_numbers').fadeIn().attr('style', 'display: block; border: none !important; width: 100%; background-color: purple !important; color: white;');
                                                        show_statistics();
                                                        $('.celebration-btn').click();
                                                    }, 1000);

                                                }

                                                if (res.lost) {
                                                    $('#gameData').html(res.content);
                                                    $('#guesses_data').html(res.content);

                                                    setTimeout(function() {
                                                        show_statistics();
                                                    }, 1000);
                                                }

                                                if (!res.won && !res.lost) {
                                                    $('#gameData').html(res.content);
                                                    $('#guesses_data').html(res.content);
                                                }

                                                currentRow++;
                                                rowTileNumber = 1;
                                                stylize_numpad();

                                            });

                                    });
                            }
                        }
                    }
                }
            });

            $.get('gameData.php', {
                    user: user,
                    currentRow: currentRow
                })
                .done(function(data) {

                    var res = JSON.parse(data);
                    if (res.won) {
                        isPlay = 0;
                        $('#gameData').html(res.content);
                        $('#guesses_data').html(res.content);
                        $('#submit_numbers').fadeIn().attr('style', 'display: block; border: none !important; width: 100%; background-color: purple !important; color: white;');

                        show_statistics();
                    }
                    if (res.lost) {
                        $('#gameData').html(res.content);
                        $('#guesses_data').html(res.content);
                        show_statistics();
                    }
                    if (!res.won && !res.lost) {
                        $('#gameData').html(res.content);
                        $('#guesses_data').html(res.content);
                    }

                    stylize_numpad();

                });

            $('[data-dismiss="statistics"]').click(function() {
                $('#statistics').modal('hide');
            });

            $('[data-dismiss="help"]').click(function() {
                $('#help').modal('hide');
            });

            $('[data-dismiss="settings"]').click(function() {
                $('#settings').modal('hide');
            });

            //when modal opens
            $('#statistics').on('show.bs.modal', function(e) {
                $(".pageContent").css({
                    opacity: 0
                });
            })

            //when modal closes
            $('#statistics').on('hidden.bs.modal', function(e) {
                $(".pageContent").css({
                    opacity: 1
                });
            })
        });
    </script>
    <script>
        var html = "";

        function submit_numbers() {
            html = $("#statistics").html();
            $("#statistics").html($("#show_pic_modal").html());
            $("#statistics").css("transform", "rotateY(180deg)");

            setTimeout(function() {
                $("#statistics").css("transform", "");
                $("#statistics").html($("#submit_numbers_modal").html());
            }, 2500);
        }

        function closeModal() {
            $(".pageContent").css({
                opacity: 1
            });
            $('#statistics').modal('hide');
        }

        function closeSubmit() {
            $(".pageContent").css({
                opacity: 1
            });

            $('#statistics').modal('hide');
            setTimeout(() => {
                $('#statistics').html(html);
            }, 1500);
        }

        function gameEndCard() {
            var element = document.querySelector('#gameData');

            // Create a copy of it
            var clone = element.cloneNode(true);

            // Update the ID and add a class
            clone.id = 'gameData2';
            clone.classList.add('text-large');

            // Inject it into the DOM
            $("#guesses_data").replaceWith(clone);
            $("#gameData2 .box-green").removeClass("animate__flipInX");
            $("#gameData2 .box-yellow").removeClass("animate__flipInX");
            $("#gameData2 .box-gray").removeClass("animate__flipInX");
            $("#gameData2 .box-green-purple").removeClass("animate__flipInX");
            $("#gameData2 .box-yellow-purple").removeClass("animate__flipInX");
            $("#gameData2 .box-yellow-purple").addClass('box-yellow').removeClass("box-yellow-purple");
            $("#gameData2 .box-green-purple").addClass('box-green').removeClass("box-green-purple");
        }

        function shareNum() {
            $("#guesses_data").load("https://guessmynumber.yilstaging.com/ #gameData");

            if (!isPlay) {
                // Global variable
                var element = $("#gameData2");

                // Global variable
                var getCanvas;

                html2canvas(element, {
                    onrendered: function(canvas) {
                        getCanvas = canvas;
                        var imgageData = canvas.toDataURL("image/png");

                        $.ajax({
                            type: 'POST',
                            data: {
                                image: imgageData
                            },
                            url: '<?= $root ?>/_image.php',
                            success: function(response) {
                                shareImage(response)
                            },
                            error: function() {
                                console.log('Something went wrong');
                            }
                        })
                    }
                });
            } else {
                shareImage('https://guessmynumber.yilstaging.com/share/logo.png');
            }
        }

        function shareImage(url) {
            const shareData = {
                title: 'Guess My Number',
                text: 'Guess My Number',
                url: url,
            }

            try {
                if (navigator.canShare &&
                    typeof navigator.canShare === 'function' &&
                    navigator.canShare(shareData)) {
                    let result = navigator.share(shareData);
                } else {
                    alert("Sharing selected data not supported.");
                }
            } catch (error) {
                console.log(error);
            }
        }

        $("#statistics > div > div > div > div:nth-child(1)").click(function() {
            $(".content").show();
            $(".content").css("display", "block");
        });

        $(document).ready(function() {
            $("#guesses_data").load("https://guessmynumber.yilstaging.com/ #gameData");

            // Global variable
            var element = $("#gameData");

            // Global variable
            var getCanvas;

            $("#btn-Preview-Image").on('click', function() {
                html2canvas(element, {
                    onrendered: function(canvas) {
                        $("#previewImage").replaceWith(canvas);
                        getCanvas = canvas;
                        var imgsrc = canvas.toDataURL("image/png");
                        console.log(imgsrc);
                        $("#newimg").attr('src', imgsrc);
                        $("#img").show();
                    }
                });
            });

            $("#btn-Convert-Html2Image").on('click', function() {
                html2canvas(element, {
                    onrendered: function(canvas) {
                        $("#previewImage").replaceWith(canvas);
                        getCanvas = canvas;
                    }
                });

                var imgageData =
                    getCanvas.toDataURL("image/png");

                // Now browser starts downloading 
                // it instead of just showing it
                var newData = imgageData.replace(
                    /^data:image\/png/, "data:application/octet-stream");

                $("#btn-Convert-Html2Image").attr(
                    "download", "critical_thinking.png").attr(
                    "href", newData);
            });

            $.get('_cron.php')
                .done(function(data) {
                    console.log('Cron Sucessfully Executed');
                });
        });
    </script>
</body>

</html>