<?php

include('config.php');

$q = $pdo->prepare('SELECT * FROM `users` WHERE `id`=?');

$q->execute(array($_POST['user']));

$user = $q->fetch(PDO::FETCH_ASSOC);

$rounds = 0;

$rounds_won = 0;

$q = $pdo->prepare('SELECT * FROM `users` WHERE `number12am`=? AND `number12pm`=?');

$q->execute(array($user['number12am'], $user['number12pm']));

foreach ($q as $row) {
    $rounds++;

    $am_pm = strtolower(date('A'));

    $parts = str_split($row['number12' . $am_pm]);

    if (strpos($row['guesses'], '"' . $parts[0] . '","' . $parts[1] . '","' . $parts[2] . '"') != false) {
        $rounds_won++;
    }
}

$winPercent = ($rounds_won / $rounds) * 100;

?>

<table class="table">

    <tr>

        <?php

        if ($user['gamesWon'] > 0) {

        ?>

            <td style="text-align: center;">

                <h1><?php echo $user['gamesWon']; ?></h1>

                Rounds Won

            </td>

        <?php

        }

        ?>

        <td style="text-align: center;">

            <h1><?php echo $user['gamesPlayed']; ?></h1>

            Rounds Played

        </td>

        <td style="text-align: center;">

            <h1><?php echo round($winPercent); ?>%</h1>

            <p style="margin-bottom: -1%;">Current Round Global Win %</p>

        </td>

    </tr>

</table>